const express = require("express");
const bodyParser = require("body-parser");
require("./database");

const PORT = process.env.PORT || 5000;

// Creates an express app.
const app = express();

// Middlewares
app.use(bodyParser.json({ limit: "30mb" }));

// Default route
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// Initiates the server.
app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
