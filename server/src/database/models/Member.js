const { Model, DataTypes } = require("sequelize");
const ShipMember = require("./ShipMember");

class Member extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        species: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: "members",
      }
    );
  }
  static associate(models) {
    this.Member = this.belongsTo(models.Ship, {
      through: ShipMember,
      foreignKey: "member_id",
    });
  }
}

module.exports = Member;
