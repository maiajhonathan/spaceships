const { Model, DataTypes } = require("sequelize");

class ShipMember extends Model {
  static init(sequelize) {
    super.init(
      {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        ship_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "Ships",
            id: "id",
          },
        },
        member_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "Members",
            id: "id",
          },
        },
      },
      {
        sequelize,
        tableName: "ship_member",
      }
    );
  }
  static associate(models) {
    ShipMember.belongsTo(models.Ship, {
      foreignKey: "ship_id",
      targetKey: "id",
      as: "Ship",
    });
    ShipMember.belongsTo(models.Member, {
      foreignKey: "member_id",
      targetKey: "id",
      as: "Member",
    });
  }
}

module.exports = ShipMember;
