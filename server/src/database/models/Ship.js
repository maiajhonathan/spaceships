const { Model, DataTypes } = require("sequelize");
const ShipMember = require("./ShipMember");

class Ship extends Model {
  static init(sequelize) {
    super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        purpose: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        tableName: "ships",
      }
    );
  }
  static associate(models) {
    this.Ship = this.belongsToMany(models.Member, {
      through: ShipMember,
      foreignKey: "ship_id",
    });
  }
}

module.exports = Ship;
