const { Sequelize } = require("sequelize");
const dbConfig = require("../config/dbConfig");

// Import Models
const Ship = require("./models/Ship");
const Member = require("./models/Member");
const ShipMember = require("./models/ShipMember");

// Instantiates Sequelize.
const connection = new Sequelize(dbConfig);

// Initiates the models passing the connection obj.
Ship.init(connection);
Member.init(connection);
ShipMember.init(connection);

// Create associations
const models = {
  Ship,
  Member,
};
ShipMember.associate(models);
Ship.associate(models);
Member.associate(models);

// Seeding
function seedTables() {
  return Promise.all([
    Ship.create({ name: "Enterprise", purpose: "Exploration" }),
    Ship.create({ name: "Planet Express", purpose: "Delivery" }),
    Member.create({ name: "Kirk", species: "Human" }),
    Member.create({ name: "Spock", species: "Hybrid" }),
    Member.create({ name: "McCoy", species: "Human" }),
    Member.create({ name: "Leela", species: "Mutant" }),
    Member.create({ name: "Fry", species: "Human" }),
    Member.create({ name: "Bender", species: "Robot" }),
  ]).catch((error) => console.log(error));
}

function setShip() {
  seedTables()
    .then(
      ([enterprise, planetexpress, kirk, spock, mccoy, leela, fry, bender]) => {
        return Promise.all([
          kirk.setShip(enterprise),
          spock.setShip(enterprise),
          mccoy.setShip(enterprise),
          leela.setShip(planetexpress),
          fry.setShip(planetexpress),
          bender.setShip(planetexpress),
        ]);
      }
    )
    .then((crew) => {
      crew.map(async (crew) => {
        var ship = await crew.getShip();
        ShipMember.create({ ship_id: ship.id, member_id: crew.id });
      });
    });
}

// Needed only once in order for the db tables to be created.
// connection.sync({ force: true }).then(() => {
//   setShip();
// });

// Very simple test on Members entity.
// connection
//   .authenticate()
//   .then(() => {
//     return Member.findOne({
//       where: {
//         name: "Leela",
//       },
//     });
//   })
//   .then(async (crew) => {
//     const ship = await crew.getShip();
//     console.log(`${crew.name}'s ship is the ${ship.name}`);
//   })
//   .catch((error) => console.log(error));

// Very simple test on Ship entity.
// connection
//   .authenticate()
//   .then(() => {
//     return Ship.findOne({
//       where: {
//         name: "Enterprise",
//       },
//     });
//   })
//   .then(async (ship) => {
//     var crew = await ship.getMembers();
//     crew = crew.map((crew) => crew.name);
//     console.log(`The ${ship.name} members are: ${crew}`);
//   })
//   .catch((error) => console.log(error));

module.exports = connection;
