module.exports = {
  username: "test",
  password: "123456",
  database: "spaceshipsdb",
  host: "localhost",
  dialect: "postgres",
  define: {
    underscored: true,
    timestamps: true,
  },
};
